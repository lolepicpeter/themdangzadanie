//
//  DataService.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//

import Foundation
import Combine


class DataService {
    
    static let instance = DataService() // Singleton
    
    @Published var actors: [Actor] = []
    var cancellables = Set<AnyCancellable>()
    
    private init() {
        print(actors)
        getPosts()
        print(actors)
    }
    
    func getPosts() {
        
        guard let url = URL(string: "https://www.infotech.sk/Service/Service.svc/getData") else {
            print("no URL")
            return }
        
        downloadData(fromURL: url) { (returnedData) in
            if let data = returnedData {
                guard let actors = try? JSONDecoder().decode(ActorList.self, from: data) else {
                    print("error parsing data")
                    return }
                DispatchQueue.main.async { [weak self] in
                    self?.actors = actors.actors
                }
            } else {
                print("No data returned.")
            }
        }
    }
    
    func downloadData(fromURL url: URL, completionHandler: @escaping (_ data: Data?) -> ()) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let body : [String: AnyHashable] = ["APIKey":"1ff7e612-52ec-4bf0-899a-3bb29b07a43c"]
        request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
        
        
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard
                let data = data,
                error == nil,
                let response = response as? HTTPURLResponse,
                response.statusCode >= 200 && response.statusCode < 300 else {
                print("Error downloading data.")
                completionHandler(nil)
                return
            }
            
            completionHandler(data)
            
        }.resume()
        
    }
    
    
}



