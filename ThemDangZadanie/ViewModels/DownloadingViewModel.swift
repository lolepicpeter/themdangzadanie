//
//  DownloadingViewModel.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//

import Foundation
import Combine

class DownloadingViewModel: ObservableObject {
    
    @Published var dataArray: [Actor] = []
    var cancellables = Set<AnyCancellable>()

    let dataService = DataService.instance
    
    init() {
        addSubscribers()
        print(dataArray)
    }
    
    func addSubscribers() {
        dataService.$actors
            .sink { [weak self] (returnedPhotoModels) in
                self?.dataArray = returnedPhotoModels
            }
            .store(in: &cancellables)
    }
    
}
