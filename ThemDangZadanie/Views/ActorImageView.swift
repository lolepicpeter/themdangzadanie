//
//  ActorPhotoView.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//

import SwiftUI

struct ActorImageView: View {
    
    @StateObject var loader: DownloadingImageViewModel
    
    init(url: String, key: String) {
        _loader = StateObject(wrappedValue: DownloadingImageViewModel(url: url, key: key))
    }
    
    var body: some View {
        ZStack {
            if loader.isLoading {
                ProgressView()
            } else if let image = loader.image {
                Image(uiImage: image)
                    .resizable()
                    .clipShape(Circle())
            }
        }
    }
}

struct DownloadingImageView_Previews: PreviewProvider {
    static var previews: some View {
        ActorImageView(url: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Brad_Pitt_2019_by_Glenn_Francis.jpg/230px-Brad_Pitt_2019_by_Glenn_Francis.jpg", key: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Brad_Pitt_2019_by_Glenn_Francis.jpg/230px-Brad_Pitt_2019_by_Glenn_Francis.jpg")
            .frame(width: 75, height: 75)
            .previewLayout(.sizeThatFits)
    }
}
