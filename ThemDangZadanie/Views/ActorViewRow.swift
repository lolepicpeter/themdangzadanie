//
//  ActorView.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//
import SwiftUI

struct ActorViewRow: View {
    
    let actor: Actor
    
    var body: some View {
        HStack {
            ActorImageView(url: actor.image, key: actor.image)
                .frame(width: 75, height: 75)
            VStack(alignment: .leading) {
                Text(actor.name)
                    .font(.headline)
                Text(actor.description)
                    .foregroundColor(.gray)
                    .italic()
            }
            .frame(maxWidth: .infinity, alignment: .leading)
        }
    }
}

struct DownloadingImagesRow_Previews: PreviewProvider {
    static var previews: some View {
        ActorViewRow(actor: Actor(children: "", country: .england, description: "William Bradley 'Brad' Pitt is an American actor and film producer. He has received a Golden Globe Award, a Screen Actors Guild Award, and three Academy Award nominations in acting categories", dob: "", gender: .male, height: "", image: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Brad_Pitt_2019_by_Glenn_Francis.jpg/230px-Brad_Pitt_2019_by_Glenn_Francis.jpg", name: "Brad Pitt", spouse: "", wiki: ""))
    }
}
