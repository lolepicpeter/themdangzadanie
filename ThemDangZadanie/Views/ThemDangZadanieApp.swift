//
//  ThemDangZadanieApp.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//

import SwiftUI

@main
struct ThemDangZadanieApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
