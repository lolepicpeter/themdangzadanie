//
//  DetailView.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 01/02/2023.
//

import SwiftUI

struct DetailView: View {
    let actor: Actor
    @State var color: Color = .white

    var body: some View {
        ZStack {
            color.ignoresSafeArea()
            HStack {
                ActorImageView(url: actor.image, key: actor.image)
                    .frame(width: 75, height: 75)
                VStack(alignment: .leading) {
                    Text(actor.name)
                        .font(.headline)
                    Text(actor.description)
                        .foregroundColor(.gray)
                        .italic()
                }
                .frame(maxWidth: .infinity, alignment: .leading)
            }
            
        }.navigationBarItems(trailing: Button(action: {
            color = Color(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1))
        }, label: {
            Text("Change Background")
        }))
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(actor: Actor(children: "", country: .england, description: "", dob: "", gender: .male, height: "", image: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Brad_Pitt_2019_by_Glenn_Francis.jpg/230px-Brad_Pitt_2019_by_Glenn_Francis.jpg", name: "Brad Pitt", spouse: "", wiki: ""))
    }
}
