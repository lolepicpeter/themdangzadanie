//
//  LoginView.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 01/02/2023.
//

import SwiftUI

struct LoginView: View {
    @AppStorage("username") var usernameSaved: String?
    @AppStorage("password") var passwordSaved: String?
    @State var username: String = ""
    @State var password: String = ""
    @State var isloggedIn: Bool = false
    @State var progressViewShow = false
    var body: some View {
        ZStack {
            switch isloggedIn {
            case false :
                VStack() {
                    TextField("username", text: $username)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(10)
                        .padding(.horizontal)
                    
                    SecureField("password", text: $password)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(10)
                        .padding(.horizontal)
                    Button("Login") {
                        UserDefaults.standard.set(username, forKey: "username")
                        UserDefaults.standard.set(password, forKey: "password")
                        progressViewShow.toggle()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            isloggedIn.toggle()

                        }
                        
                    }
                    if progressViewShow {
                        ProgressView()
                        
                    }
                    
                }.onAppear {
                    username = UserDefaults.standard.string(forKey: "username") ?? ""
                    password = UserDefaults.standard.string(forKey: "password") ?? ""
                }
            default :
                ContentView()
            }
        }
        
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
