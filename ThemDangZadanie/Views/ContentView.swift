//
//  ContentView.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var vm = DownloadingViewModel()
    
    var body: some View {
        
        
        NavigationView {
            List {
                ForEach(vm.dataArray, id: \.hashValue) { model in
                    NavigationLink {
                        DetailView(actor: model)

                    } label: {
                        ActorViewRow(actor: model)
                            
                    }
                }
            }
            .navigationTitle("Downloading Images!")
        }
    }
}

struct DownloadingImagesBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

