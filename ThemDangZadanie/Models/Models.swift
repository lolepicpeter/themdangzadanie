//
//  Models.swift
//  ThemDangZadanie
//
//  Created by Them Dang on 31/01/2023.
//

import Foundation

// MARK: - Empty
struct ActorList: Codable {
    let actors: [Actor]
}

// MARK: - Actor
struct Actor: Codable, Hashable {
    
    let children: String
    let country: Country
    let description, dob: String
    let gender: Gender
    let height: String
    let image: String
    let name, spouse: String
    let wiki: String
}

enum Country: String, Codable {
    case england = "England"
    case unitedStates = "United States"
}

enum Gender: String, Codable {
    case female = "female"
    case male = "male"
}
